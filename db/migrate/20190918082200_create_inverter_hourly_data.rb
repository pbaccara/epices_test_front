class CreateInverterHourlyData < ActiveRecord::Migration[6.0]
  def change
    create_table :inverter_hourly_data do |t|
      t.references :inverter
      t.datetime :produced_at
      t.integer :energy

      t.timestamps
    end

    add_index :inverter_hourly_data,
      [:inverter_id, :produced_at],
      unique: true,
      name: 'unique_inverter_hourly_data'
  end
end
