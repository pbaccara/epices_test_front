class Inverter < ApplicationRecord
  belongs_to :pv_system

  has_many :inverter_hourly_datum
  alias :hourly_datum :inverter_hourly_datum
end
