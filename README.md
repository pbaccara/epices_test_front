# Sujet
Une installation photovoltaïque dispose d'un appareil appelé "datalogger" qui nous transmet chaque jour des données sur la production de la veille.

Cette installation photovoltaïque est composée de 2 onduleurs.

Les données consolidées contiennent la production horaire de chaque onduleur.

Le but de l'application est, à partir des données de production, de disposer des fonctionnalités suivantes :

- Afficher les données horaires de chaque onduleur.
- Afficher la somme d'énergie produite sur la journée pour l'ensemble du système.
- Le tout dans une interface web fonctionnelle.

**Vous n'avez pas de contraintes visuelles, ni fonctionnelles, à vous de réaliser ce qui vous semble pertinent comme outils d'après le besoin exprimé.**

# Nous observerons
La qualité et l'organisation du code en général
La qualité de l'interface

**Contrainte :** essayer de réaliser ce projet dans le temps imparti (~4 heures).

# Réaliser le projet à l'aide du présent starter :

- branche **master** *(Ruby/Rails)*
- branche **webix** *(Ruby/Rails/Webix/Highcharts)*

**Mettre le code à disposition sur un dépôt git et nous transmettre les moyens d'y accéder.**