class CreatePvSystems < ActiveRecord::Migration[6.0]
  def change
    create_table :pv_systems do |t|
      t.string :name

      t.timestamps
    end
  end
end
