# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_18_082200) do

  create_table "inverter_hourly_data", force: :cascade do |t|
    t.integer "inverter_id"
    t.datetime "produced_at"
    t.integer "energy"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["inverter_id", "produced_at"], name: "unique_inverter_hourly_data", unique: true
    t.index ["inverter_id"], name: "index_inverter_hourly_data_on_inverter_id"
  end

  create_table "inverters", force: :cascade do |t|
    t.integer "pv_system_id"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["pv_system_id"], name: "index_inverters_on_pv_system_id"
  end

  create_table "pv_systems", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
