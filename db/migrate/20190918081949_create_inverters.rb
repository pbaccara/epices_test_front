class CreateInverters < ActiveRecord::Migration[6.0]
  def change
    create_table :inverters do |t|
      t.references :pv_system
      t.string :name

      t.timestamps
    end
  end
end
