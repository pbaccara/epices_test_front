pv_system = PvSystem.create(name: 'Site 1')

inverters = []
3.times do |i|
  inverters << Inverter.create(pv_system: pv_system, name: "Inverter 1")
end

[
  [
    [0, 24, 910, 2320, 3480, 4293, 4752, 4821, 4585, 3752, 2823, 1980, 877, nil, nil, nil],
    [0, 14, 792, 2250, 3454, 4225, 4682, 4813, 4568, 3745, 2801, 1962, 863, nil, nil, nil],
    [0, 18, 908, 2629, 3627, 4417, 4867, 4931, 4742, 4014, 2882, 1989, 830, 141, nil, nil]
  ],
  [
    [0, 24, 876, 2377, 3642, 4495, 4986, 5010, 4774, 4247, 3422, 2244, 968, nil, nil, nil],
    [0, 24, 795, 2317, 3588, 4399, 4834, 4997, 4755, 4252, 3435, 2245, 957, nil, nil, nil],
    [0, 17, 933, 2682, 3839, 4718, 5225, 5287, 5098, 4568, 3584, 2340, 942, 146, nil, nil]
  ],
  [
    [0, 32, 960, 2530, 3887, 4805, 5342, 5419, 5158, 4568, 3639, 2365, 1012, nil, nil, nil],
    [0, 21, 835, 2443, 3821, 4704, 5225, 5382, 5133, 4546, 3640, 2363, 1000, nil, nil, nil],
    [0, 25, 969, 2812, 4072, 5010, 5540, 5626, 5414, 4836, 3785, 2448, 978, 153, nil, nil]
  ]
].each_with_index do |inverter_daily_datum, i|
  inverter_daily_datum.each_with_index do |daily_datum, j|
    daily_datum.each_with_index do |energy, k|
      inverters[i].inverter_hourly_datum.create(produced_at: Time.utc(2019, 9, j + 1 , 5 + k), energy: energy)
    end
  end
end